import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { TabsLogPage } from '../pages/tabs-log/tabsLog';
import { HomeUsuarioPage } from '../pages/home-usuario/home-usuario';
import { MiPerfilPage } from "../pages/mi-perfil/mi-perfil";
import { MisPublicacionesPage } from "../pages/mis-publicaciones/mis-publicaciones";
import { BusquedaPage } from "../pages/busqueda/busqueda";
import { ComentariosPage } from '../pages/comentarios/comentarios';
import { PublicacionPage } from "../pages/publicacion/publicacion";

//Servicios
import { ComentarioService } from "./Services/comentario.service";
import { PublicacionService } from "./Services/publicacion.service";
import { UsuarioService } from "./Services/usuario.service";
import { HttpModule } from "@angular/http";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@NgModule({
  declarations: [
    MyApp,
    ContactPage,
    HomePage,
    TabsPage,
    TabsLogPage,
    ComentariosPage,
    HomeUsuarioPage,
    MiPerfilPage,
    PublicacionPage,
    MisPublicacionesPage,
    BusquedaPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
     MyApp,
    ContactPage,
    HomePage,
    TabsPage,
    TabsLogPage,
    ComentariosPage,
    PublicacionPage,
    HomeUsuarioPage,
    MiPerfilPage,
    MisPublicacionesPage,
    BusquedaPage
  ],
  providers: [
    StatusBar,
    ComentarioService,
    PublicacionService,
    UsuarioService,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
