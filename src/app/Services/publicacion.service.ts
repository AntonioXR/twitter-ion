import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class PublicacionService {
    listPublicaciones:Array<any>=[]
    listPublicacionesYo:Array<any>=[]
    private uri = "http://localhost:3000/api/publicacion/"
    constructor(private http: Http) { }
    getPublicaciones(){
        return this.http.get(this.uri).map(res=>this.listPublicaciones = res.json())
    }
    getPublicacionesYo(){
        return this.http.get(this.uri+`yo/${localStorage.getItem("UDI")}`).map(res=> this.listPublicacionesYo =res.json())
    }
    insert(data:any,callback){
        console.log(data);
        
        this.http.post(this.uri, data).toPromise()
        .then(res=>{
            callback(res.json())
         })
         .catch(res=>{
            console.log(res);
            callback(false)
         })
    }
    update(data:any, callback){
        console.log(data);
        this.http.put(this.uri+data.idPublicacion,data).toPromise()
        .then(res=>{
            callback(res.json())
         })
         .catch(res=>{
            console.log(res);
            callback(false)
         })
    }
    delete(idPublicacion:any, callback){
        console.log(idPublicacion);
        this.http.delete(this.uri+idPublicacion).toPromise()
        .then(res=>{
            callback(res.json())
         })
         .catch(res=>{
            console.log(res);
            callback(false)
         })
    }
        upload(data, callback){
            var form = new FormData()
            form.append("file", data, data.fileName)
            console.log(data);
            
            this.http.post("http://localhost:3000/upload", form).toPromise()
            .then(res=>{
                callback(res.json())
            })
            .catch(res=>{
                console.log(res);
                callback(false)
            })
        }
}