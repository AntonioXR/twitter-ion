import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class ComentarioService {
    listComentarios:Array<any>=[]
    private uri = "http://localhost:3000/api/comentario/"
    constructor(private http: Http) { }
    getComentarios(idPublicacion){
        return this.http.get(this.uri+idPublicacion).map(res=>this.listComentarios = res.json())
    }
    insert(data:any, callback){
        this.http.post(this.uri, data).toPromise()
        .then(res=>{
            callback(res.json())
         })
         .catch(res=>{
            console.log(res);
            callback(false)
         })
    }
    delete(idComentario:any, callback){
        console.log(idComentario, this.uri+idComentario);
        
        this.http.delete(this.uri+idComentario).toPromise()
        .then(res=>{
            callback(res.json())
         })
         .catch(res=>{
            console.log(res);
            callback(false)
         })
    }
}