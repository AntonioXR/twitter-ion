import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class UsuarioService {
    private uri = "http://localhost:3000/api/usuario/"
    constructor(private http: Http) {

     }
     autenticar(data:any, callback){
         this.http.post(this.uri+"autenticar", data).toPromise()
         .then(res=>{
            callback(res.json())
         })
         .catch(res=>{
            console.log(res);
            callback({state:false})
         })
     }
    registrar(data:any, callback){
        this.http.post(this.uri, data).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            console.log(res);
            callback(false)
        })
    }
    modificar(data:any, callback){
        console.log(data, localStorage.getItem("UDI"));
        
        this.http.put(this.uri+localStorage.getItem("UDI"), data).toPromise()
        .then(res=>{
            callback(res.json())
        })
        .catch(res=>{
            console.log(res);
            callback(false)
        })
    }

    upload(data, callback){
            var form = new FormData()
            form.append("file", data, data.fileName)
            console.log(data);
            
            this.http.post("http://localhost:3000/upload", form).toPromise()
            .then(res=>{
                callback(res.json())
            })
            .catch(res=>{
                console.log(res);
                callback(false)
            })
        }
}