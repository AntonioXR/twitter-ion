import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ToastController } from 'ionic-angular';
import { PublicacionService } from "../../app/Services/publicacion.service";
import { HomeUsuarioPage } from "../home-usuario/home-usuario"
@Component({
  selector: 'page-publicacion',
  templateUrl: 'publicacion.html',
})
export class PublicacionPage {
  publicacionTemp = {idUsuario:"",file:"",imagen:"", idPublicacion:"", AG:false}
  constructor(private _publicacion:PublicacionService,private _toast:ToastController ,public navCtrl: NavController, public navParams: NavParams) {
    if(this.navParams.get("data") != "AG"){
      this.publicacionTemp = this.navParams.get("data");
    }else{
      this.publicacionTemp.AG = true
    }
  }
  cargarIMG($event){
      let file = $event.target.files[0]
      this.publicacionTemp.file = file;
  }
  upload(call){
    this._publicacion.upload(this.publicacionTemp.file, resFL=>{
      call(resFL.Mensaje)
    })
  }
  editar(){
  if(this.publicacionTemp.file != null){
    this.upload(res=>{
    this.publicacionTemp.imagen = res
    this.opreaciones("ED")
    })    
   }else{
    this.opreaciones("ED")
   }         
  }
  agregar(){
    this.publicacionTemp.idUsuario = localStorage.getItem("UDI")
   if(this.publicacionTemp.file != null || this.publicacionTemp.file != ""){
    this.upload(res=>{
    this.publicacionTemp.imagen = res
    this.opreaciones("AG")
    })    
   }else{
     this.publicacionTemp.imagen =null;
    this.opreaciones("AG")
   }      
  }
  eliminar(){
   if(this.publicacionTemp.file != null){
    this.upload(res=>{
    this.publicacionTemp.imagen = res
    this.opreaciones("EL")
    })    
   }else{
    this.opreaciones("EL")
   }      
  }
  opreaciones(OP){
    switch(OP){
      case "AG":
        this._publicacion.insert(this.publicacionTemp, res=>{
        if(res.Mensaje){
            this.toast("Se Agrego la publicacion con exito")
            this.navCtrl.popTo(HomeUsuarioPage);
          }else{
            this.toast("Oh oh tuvimos un error al agregar")
          }
        })
      break;
      case "ED":
        this._publicacion.update(this.publicacionTemp, res=>{
        if(res.Mensaje){
              this.toast("Se edito la publicacion con exito")
              this.navCtrl.popTo(HomeUsuarioPage);
            }else{
              this.toast("Oh oh tuvimos un error al editar")
            }
          })
      break;
      case "EL":
        this._publicacion.delete(this.publicacionTemp.idPublicacion, res=>{
        if(res.Mensaje){
          this.toast("Se eliminar la publicacion con exito")
          this.navCtrl.popTo(HomeUsuarioPage);
        }else{
          this.toast("Oh oh tuvimos un error al eliminar")
        }
      })
      break;
    }
  }
  toast(Mensaje){
    let toast = this._toast.create({message:Mensaje,duration: 3000,showCloseButton:true,position:"top"})
    toast.present()
  }
}
