import { Component } from '@angular/core';

import { HomeUsuarioPage } from '../home-usuario/home-usuario';
import { MiPerfilPage } from "../mi-perfil/mi-perfil";
import { MisPublicacionesPage } from "../mis-publicaciones/mis-publicaciones";
import { BusquedaPage } from "../busqueda/busqueda";

@Component({
  templateUrl: 'tabs-log.html'
})
export class TabsLogPage {

  tab1Root = HomeUsuarioPage
  tab2Root = MiPerfilPage
  tab3Root = MisPublicacionesPage
  tab4Root = BusquedaPage
  constructor() {

  }
}
