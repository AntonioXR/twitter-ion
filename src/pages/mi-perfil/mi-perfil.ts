import { Component } from '@angular/core';
import { NavController, NavParams, ToastController,App } from 'ionic-angular';
import { UsuarioService } from "../../app/Services/usuario.service";
import { TabsPage } from "../tabs/tabs";

@Component({
  selector: 'page-mi-perfil',
  templateUrl: 'mi-perfil.html',
})
export class MiPerfilPage {
	
	usuarioTemp={
		nombre:localStorage.getItem("UNA"),
		idUsuario: localStorage.getItem("UDI"),
		nick: localStorage.getItem("UNI"),
    imagen: localStorage.getItem("IMG"),
    file: ""
  }
  constructor(private app:App,private _toast:ToastController, private _usuario:UsuarioService, public navCtrl: NavController, public navParams: NavParams) {
  }
  cargarIMG($event){
      let file = $event.target.files[0]
      this.usuarioTemp.file = file;
  }

  modificar(){
    if(this.usuarioTemp.file != null, this.usuarioTemp.file != ""){
      this.imagen();
    }else{
      this.nonImagen()
    }
  }
  
  nonImagen(){
   this._usuario.modificar(this.usuarioTemp, res=>{
          if(res.Mensaje){
            var toast = this._toast.create({position:"top", message:"Se Modifico el Perfil"})
            toast.present()
          }else{
            var toast = this._toast.create({position:"top", message:"No sepudo cambiar el perfil"})
            toast.present()
          }
      })
  }

  imagen(){
     this._usuario.upload(this.usuarioTemp.file, res=>{
       this.usuarioTemp.imagen = res.Mensaje
        this._usuario.modificar(this.usuarioTemp, res=>{
          if(res.Mensaje){
            var toast = this._toast.create({position:"top", message:"Se Modifico el Perfil"})
            toast.present()
            localStorage.setItem("UDI", this.usuarioTemp.idUsuario)
            localStorage.setItem("UNA", this.usuarioTemp.nombre)
            localStorage.setItem("UNI", this.usuarioTemp.nick)
            localStorage.setItem("IMG", this.usuarioTemp.imagen)
          }else{
            var toast = this._toast.create({position:"top", message:"No sepudo cambiar el perfil"})
            toast.present()
          }
      })
     })
  }


  ionViewWillEnter() {
  }
  cs(){
    localStorage.clear() 
    this.app.getRootNav().setRoot(TabsPage)
   }
}
