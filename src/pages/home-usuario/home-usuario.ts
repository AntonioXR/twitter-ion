import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular'
import { PublicacionService } from "../../app/Services/publicacion.service"
import { ComentariosPage } from "../comentarios/comentarios"
import { PublicacionPage } from "../publicacion/publicacion";
@Component({
  selector: 'page-home-usuario',
  templateUrl: 'home-usuario.html',
})
export class HomeUsuarioPage {
  UDI = localStorage.getItem("UDI")
  constructor(private _modal:ModalController,private _publicacion:PublicacionService,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewWillEnter() {
   this._publicacion.getPublicaciones().subscribe()
  }
  comentar(data){
    let modal = this._modal.create(ComentariosPage, {data})
    modal.present();
  }
  publicacion(data){
    this.navCtrl.push(PublicacionPage, {data})
  }
}
