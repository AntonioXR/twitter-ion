import { Component } from '@angular/core';
import { NavController, NavParams,ViewController,ToastController, AlertController } from 'ionic-angular';
import { ComentarioService } from '../../app/Services/comentario.service'

@Component({
  selector: 'page-comentarios',
  templateUrl: 'comentarios.html'
})
export class ComentariosPage {
  UDI = localStorage.getItem("UDI")
  comentarioTemp={comentario:"", idUsuario : localStorage.getItem("UDI"), idPublicacion: 0, idComentario:0}
  constructor(private _alert:AlertController, private _toast:ToastController ,private viewCtrl:ViewController, private _comentario:ComentarioService,public navCtrl: NavController, public navParams: NavParams) {
    this.cargarData(this.navParams.get("data"))
    
  }
  cargarData(data){
    this.comentarioTemp.idPublicacion = data.idPublicacion;
  	this._comentario.getComentarios(data.idPublicacion).subscribe();
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }
  agregar(){
    console.log(this.comentarioTemp);
    
    this._comentario.insert(this.comentarioTemp, res=>{
        if(!res.Mensaje){
            var toast = this._toast.create({message:"Oh ho tenemos problemas", duration: 3000, showCloseButton:true})
            toast.present()
            this.navCtrl.popTo(ComentariosPage)
            this.comentarioTemp.comentario = ""
        }
        this.cargarData(this.navParams.get("data"))
    })
  }
  eliminar(ID){
    this.comentarioTemp.idComentario = ID
    console.log(this.comentarioTemp);
    
    var alert = this._alert.create({
      title: "Eliminar",
      message: "Desea eliminar este comentario?",
      buttons: [
        {
          text: "Cancelar"
        },
        {
          text:"Eliminar",
          handler: _=>{
            this._comentario.delete(this.comentarioTemp.idComentario, res=>{
               if(!res.Mensaje){
                    var toast = this._toast.create({message:"Oh ho tenemos problemas" ,duration: 3000, showCloseButton:true})
                    toast.present()
                    this.navCtrl.popTo(ComentariosPage)
                    this.comentarioTemp.idComentario = 0;
                    this.comentarioTemp.comentario = "";
                }
                this.cargarData(this.navParams.get("data"))
            })
          }
        }
      ]
    })
    alert.present()
  }
}
