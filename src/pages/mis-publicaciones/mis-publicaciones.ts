import { Component } from '@angular/core'
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular'
import { PublicacionService } from "../../app/Services/publicacion.service"
import { ComentariosPage } from "../comentarios/comentarios"
import { PublicacionPage } from "../publicacion/publicacion";

@Component({
  selector: 'page-mis-publicaciones',
  templateUrl: 'mis-publicaciones.html',
})
export class MisPublicacionesPage {

 constructor(private _modal:ModalController,private _publicacion:PublicacionService,public navCtrl: NavController, public navParams: NavParams) {
  }


  ionViewDidLoad() {
    this._publicacion.getPublicacionesYo().subscribe()
  }
  comentar(data){
    let modal = this._modal.create(ComentariosPage, {data})
    modal.present();
  }
  publicacion(data){
    this.navCtrl.push(PublicacionPage, {data})
  }

}
