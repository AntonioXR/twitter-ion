import { Component } from '@angular/core';
import { NavController,AlertController,App } from 'ionic-angular';
import { UsuarioService } from "../../app/Services/usuario.service";
import { TabsLogPage } from "../tabs-log/tabsLog";
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  userTemp ={
    nick:"",contrasena:""
  }
  constructor(public app: App,private _alert:AlertController,private _usuario:UsuarioService,public navCtrl: NavController) {
    if(localStorage.getItem("UDI") != null) {
      this.app.getRootNav().setRoot(TabsLogPage)
    }
  }
  autenticar(){
    this._usuario.autenticar(this.userTemp, res=>{
      if(res.state){
        localStorage.setItem("UDI", res.idUsuario)
        localStorage.setItem("UNA", res.nombre)
        localStorage.setItem("UNI", res.nick)
        localStorage.setItem("IMG", res.imagen)
        
        this.app.getRootNav().setRoot(TabsLogPage)
        
      }else{
        var alert = this._alert.create({
          message:"Usuario o contraseña no validos",
          title: "Upss!"
        })
        alert.present()
      }
    })
  }
  
}
